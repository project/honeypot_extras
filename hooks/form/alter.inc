<?php

/**
 * @file
 * Honeypot Extras form alter hooks.
 */

declare(strict_types=1);

/**
 * Implements hook_form_alter().
 */
function honeypot_extras_form_alter(&$form, &$formState, $formId): void {
  $patterns = variable_get('honeypot_extras_form_id_patterns');

  if (is_null($patterns)) {
    return;
  }

  $patternArray = preg_split('/\s*[,\n\r]\s*/', $patterns);

  foreach ($patternArray as $pattern) {
    if (preg_match(sprintf('/%s/', $pattern), $formId)) {
      honeypot_add_form_protection($form, $formState, ['honeypot', 'time_restriction']);
      continue;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function honeypot_extras_form_honeypot_admin_form_alter(array &$form, array $formState): void {
  $form['extras'] = [
    '#type' => 'fieldset',
    '#title' => t('Honeypot Extras'),
  ];

  $form['extras']['honeypot_extras_form_id_patterns'] = [
    '#title' => t('Additional form ID patterns'),
    '#type' => 'textarea',
    '#description' => t('Additional form IDs or regex patterns to add Honeypot protection to.'),
    '#default_value' => variable_get('honeypot_extras_form_id_patterns'),
  ];
}
